"use strict";

var canvas = document.getElementById('tetris-canvas');
// Adjusts canvas size to show on-screen controller debugging
canvas.height = 16 * (GAME_HEIGHT + 1) * SCALE;
canvas.width = 16 * GAME_WIDTH * SCALE;

var ctx = canvas.getContext('2d');
ctx.imageSmoothingEnabled = false;
ctx.scale(SCALE, SCALE);
ctx.fillStyle = '#000';
ctx.font = '12px arial';
ctx.fillRect(0,0, 16 * GAME_WIDTH, 16 * (GAME_HEIGHT + 1) + GAMEPAD_DEBUGGER_HEIGHT);

function loadSprites(blocks, cursors) {
    var i;
    for (i=1; i<=blocks.names.length; i++) {
        blocks.sprites[i] = new Image();
        blocks.sprites[i].src = 'sprites/' + blocks.names[i-1] + '.png';
    }
    GLOBAL.nrBlockSprites = blocks.names.length;

    for (i=1; i<=cursors.names.length; i++) {

        cursors.sprites[i] = new Image();
        cursors.sprites[i].src = 'sprites/' + cursors.names[i-1] + '.png';
    }
    GLOBAL.nrCursorSprites = cursors.names.length;
}

var debuggedGamepads = [];

if(DEBUG_GAMEPADS) {
    window.addEventListener("gamepadconnected", function(e) {
        while( debuggedGamepads.length < e.gamepad.index ) {
            debuggedGamepads.push(null);
        }
        debuggedGamepads[e.gamepad.index] = e.gamepad;

        canvas.height = 16 * (GAME_HEIGHT + 1) * SCALE + GAMEPAD_DEBUGGER_HEIGHT * debuggedGamepads.length;
        canvas.width = Math.max(16 * GAME_WIDTH * SCALE, GAMEPAD_DEBUGGER_WIDTH);
    });
    window.addEventListener("gamepaddisconnected", function(e) {
        debuggedGamepads[e.gamepad.index] = null;
    });
}

function render() {

    // Check gamepad button state transitions
    GamepadController.updateAll();

    for( var i = 0; i < debuggedGamepads.length; i++) {
        ctx.save();
    
        // override the transform
        ctx.setTransform(1, 0, 0, 1, 0, SCALE * 16 * (GAME_HEIGHT + 1) + i * GAMEPAD_DEBUGGER_HEIGHT );
        drawGamepadDebugger(ctx, standardizeGamepad(navigator.getGamepads()[0]) );

        ctx.restore();
    }

    GLOBAL.taGame_list.forEach(function(game) {
        game.render();
    });
}

loadSprites(BLOCKS, CURSORS);
